from django.conf.urls import include, url
from user import views

urlpatterns=[
   url(r'login', views.login),
   url(r'^', views.index),
   url(r'^register', views.register),

]