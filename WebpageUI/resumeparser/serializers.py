from rest_framework import serializers
from . models import Technology
from . models import Graphs

class TechnologySerializer(serializers.ModelSerializer):

    class Meta:
        model= Technology
        fields= ('technology', 'digital','apptype','category','status','app_final')

class GraphsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Graphs
        fields = ('graph_id', 'graph_name','status')