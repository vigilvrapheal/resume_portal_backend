import csv, io
import psycopg2
import spacy
from spacy.matcher import Matcher
from .models import Technology
import textdistance

conn = psycopg2.connect(database="resume_portal", host="localhost", user="postgres", password="root")
cur = conn.cursor()

nlp_sm = spacy.load("en_core_web_sm")

def runcheckcsv1(path, date_of_CSV):
    success = 0
    if path:
        with open(path,'rt')as f:

          print(type(f))
          data = csv.reader(f)

          print(type(data))

          # Inserting the CHECK1 for the UPDATION OF THE RESUME DETAILS OF THE EMPLOYEE
          emp_query = "SELECT ibs_empid FROM resumeparser_employee;"
          cur.execute(emp_query)
          emp_status = cur.fetchall()
          # print("Existing data of the Employees present in the DATABASE", emp_status)


        # Query for calculating the Digital Technologies
          d_query = " select id from resumeparser_technology where digital = '%s'"
          item = 'YES'
          digital_query = d_query % item
          cur.execute(digital_query)
          digitals = cur.fetchall()
          dq_denom = len(digitals) * 5

          all_digital_technology = []

          for item in digitals:
              all_digital_technology.append(item[0])

          # print("Total number of the digital Technologies",len(all_digital_technology))

          next(data)
          for row in data:
              try:
                print(type(row))

                emp_id = row[12]
                empname = row[13]
                designation = row[14]
                if row[8]:
                    decimal = float(row[8]) / 12
                    # print("*************",decimal)
                    decimal_rounded = round(decimal,1)
                    decimal_rounded_final = str(decimal_rounded).split('.')[1]
                    # print("*****-----------********", decimal_rounded_final)
                    years_of_experience = row[7] + "." + str(decimal_rounded_final)
                else:
                    years_of_experience = row[7]
                tech_domain = str(row[4])
                tech_domain = tech_domain.replace('#', '')
                if float(years_of_experience) > 1:
                    about = str(years_of_experience) + " Years of experience across " + str(tech_domain) + " domain and working primarily on " + str(row[2])
                else:
                    about = "Less than a year of experience across " + str(tech_domain) + " domain and working primarily on " + str(row[2])
                try:
                    contact =float(row[0])
                    contact = int(contact)
                except:
                    contact = row[0]
                functional_group = row[1]
                emp_group = fetch_emp_group(functional_group)
                if emp_group:
                    emp_groups = emp_group[0]
                    emp_functional_group = str(emp_groups)
                else:
                    emp_functional_group = 'others'
                # print("............Inserting the Functional Group Of the Employee......",emp_functional_group)
                visa = row[5]
                visa = str(visa)
                bad_chars = [';', '#', '-', '.']
                for i in bad_chars:
                    visa = visa.replace(i, ' ')

                visa_location = fetch_visalocation(visa)
                visa_data = sort_visa_location(visa_location)
                if visa_data:
                    visa_data = str(visa_data)
                    emp_visa_location = str(visa_data)
                else:
                    emp_visa_location = None
                # print("............VISA LOCATION......", emp_visa_location)
                emp_check = 0
                # CHECKING FOR THE emp data in the DB
                for emp in emp_status:
                    emp = ''.join(emp)
                    # print("!!!!", emp, "@@@@", emp_id)
                    if emp == emp_id:
                        # print("Duplication found...........NOW CHECKING FOR THE DATE ")
                        date_query = "SELECT date_of_resume,id FROM resumeparser_employee where ibs_empid = '%s';"
                        item = emp_id
                        date_result = date_query % item
                        cur.execute(date_result)
                        date_of_resumes_result = cur.fetchall()
                        emp_check = emp_check + 1
                        for date, e_id in date_of_resumes_result:
                            if date < date_of_CSV:
                                # print("Date entered is a greater than the resume date..........")
                                delete_query = "DELETE FROM resumeparser_employee_skills where employee_id=%s; DELETE FROM resumeparser_employee where id = %s;"
                                delete_db = delete_query % (e_id, e_id)
                                cur.execute(delete_db)
                                # print("AFTER THE DELETING THE OLD RECORD UPDATING IT WITH THE NEW RECORD")
                                insert_query = "INSERT INTO resumeparser_employee(ibs_empid,emp_name,designation,about,contact,status, date_of_resume, functional_group, visa, visa_location)VALUES(%s,%s,%s,%s,%s,%s, %s, %s, %s, %s)"
                                record_insert = (emp_id, empname, designation, about, contact, True, date_of_CSV, emp_functional_group, visa, emp_visa_location)
                                cur.execute(insert_query, record_insert)
                                conn.commit()
                                success = 1
                                select_id = "select id from resumeparser_employee where ibs_empid='%s'"
                                select_id = select_id % emp_id
                                cur.execute(select_id)
                                employee_id = cur.fetchall()
                                emp_id_int = employee_id[0][0]
                                # Getting the employee ID from the Resume

                                tech = row[3]
                                new_tech = tech.replace("#", " ")
                                tech_list = new_tech.split(';')
                                new_tech_list = []
                                for tech in tech_list:
                                    if "," in tech:
                                        new_tech_list.extend(tech.split(","))
                                new_tech_list.extend(tech_list)
                                emp_tech_ids = []
                                emp_digitals_count = 0

                                for tech in new_tech_list:
                                    tech2 = tech.strip()
                                    tech_query = "select id,digital from resumeparser_technology where upper(technology) = upper('%s') "
                                    tech_query = tech_query % tech2
                                    cur.execute(tech_query)
                                    techid = cur.fetchall()
                                    ids = list(techid)
                                    for id in ids:
                                        x = "select id from resumeparser_employee_skills where employee_id='%s' and technology_id='%s'"
                                        record = (emp_id_int, id[0])
                                        cur.execute(x, record)
                                        data = cur.fetchall()
                                        if data is None or len(data) == 0:
                                            insert = "INSERT INTO resumeparser_employee_skills(employee_id,technology_id)VALUES(%s,%s)"
                                            record = (emp_id_int, id[0])
                                            cur.execute(insert, record)
                                            conn.commit()
                                        if id[1] == 'YES':
                                            emp_digitals_count = emp_digitals_count + 1
                                if emp_digitals_count > 0:
                                    dq_numerator = emp_digitals_count * 5
                                    digital_quotient = dq_numerator / dq_denom
                                    digital_quotient = digital_quotient * 100
                                    dq = round(digital_quotient, 2)
                                    update_dq_query = "UPDATE resumeparser_employee set  digital_quotient='%s' where id='%s'"
                                    record = (dq, emp_id_int)
                                    cur.execute(update_dq_query, record)
                                # print("Successful entered the new set of the emp records")
                            # else:
                                # print("OUT of the IF Loop i.e Date entered is not greater than the DB Date")
                                # print("As the date is not the latest date as compared from the resume.... NO ACTION")
                    # else:
                    #     print("When the emp_id doesnt exist..........Following the usual procedure")

                if emp_check == 0:
                    insert_query = "INSERT INTO resumeparser_employee(ibs_empid,emp_name,designation,about,contact,status, date_of_resume, functional_group, visa, visa_location)VALUES(%s,%s,%s,%s,%s,%s, %s, %s, %s, %s)"
                    record_insert = (emp_id, empname, designation, about, contact, True, date_of_CSV, emp_functional_group, visa, emp_visa_location)
                    cur.execute(insert_query, record_insert)
                    conn.commit()
                    success = 1
                    select_id = "select id from resumeparser_employee where ibs_empid='%s'"
                    select_id = select_id % emp_id
                    cur.execute(select_id)
                    employee_id = cur.fetchall()
                    emp_id_int = employee_id[0][0]
                    # Getting the employee ID from the Resume

                    tech = row[3]
                    new_tech = tech.replace("#", " ")
                    tech_list = new_tech.split(';')
                    new_tech_list =[]
                    for tech in tech_list :
                        if "," in tech :
                            new_tech_list.extend(tech.split(","))
                    new_tech_list.extend(tech_list)
                    emp_tech_ids = []
                    emp_digitals_count = 0
                    tech_error = []
                    spell_alert = 0
                    for tech in new_tech_list:
                          tech2 = tech.strip()
                          technology_data = list(Technology.objects.order_by('id'))
                          for tech_org in technology_data:
                              similarityPercent = textdistance.levenshtein.distance(tech2, str(tech_org))
                              print(similarityPercent)
                              if similarityPercent > 0 and similarityPercent < 3:
                                  spell_alert = 1
                                  break;
                          if spell_alert == 1:
                              tech2 = str(tech_org)
                          tech_query = "select id,digital from resumeparser_technology where upper(technology) = upper('%s') "
                          tech_query = tech_query % tech2
                          cur.execute(tech_query)
                          techid = cur.fetchall()
                          ids = list(techid)
                          tech_check = 0
                          tech_check = len(ids)
                          if tech_check == 0:
                              tech_error.append(tech)
                          for id in ids:
                              x = "select id from resumeparser_employee_skills where employee_id='%s' and technology_id='%s'"
                              record = (emp_id_int, id[0])
                              cur.execute(x, record)
                              data = cur.fetchall()
                              if data is None or len(data) == 0:
                                      insert = "INSERT INTO resumeparser_employee_skills(employee_id,technology_id)VALUES(%s,%s)"
                                      record = (emp_id_int, id[0])
                                      cur.execute(insert, record)
                                      conn.commit()
                              if id[1] == 'YES':
                                   emp_digitals_count = emp_digitals_count + 1
                    print("All the tech that is not inserted into the DB",tech_error,"-----------*********-------")
                    if tech_error:
                        insert = "INSERT INTO resumeparser_employee_erro_log(ibs_empid,tech_error)VALUES(%s,%s)"
                        record = (emp_id, tech_error)
                        cur.execute(insert, record)
                        conn.commit()
                    if emp_digitals_count > 0:
                            dq_numerator = emp_digitals_count * 5
                            digital_quotient = dq_numerator / dq_denom
                            digital_quotient = digital_quotient * 100
                            dq = round(digital_quotient,2)
                            update_dq_query = "UPDATE resumeparser_employee set  digital_quotient='%s' where id='%s'"
                            record = (dq, emp_id_int)
                            cur.execute(update_dq_query,record)
                            conn.commit()
                    # print("Successful entered the new set of the emp records")
              except:
                  print(row)
                  continue
    return success

def runcheckcsv(file, date_of_CSV):
    success = 0
    if file:
          file.open()
          data_io = io.TextIOWrapper(file, encoding= 'unicode_escape')
          data = csv.reader(data_io)

          # Inserting the CHECK1 for the UPDATION OF THE RESUME DETAILS OF THE EMPLOYEE
          emp_query = "SELECT ibs_empid FROM resumeparser_employee;"
          cur.execute(emp_query)
          emp_status = cur.fetchall()
          # print("Existing data of the Employees present in the DATABASE", emp_status)


        # Query for calculating the Digital Technologies
          d_query = " select id from resumeparser_technology where digital = '%s'"
          item = 'YES'
          digital_query = d_query % item
          cur.execute(digital_query)
          digitals = cur.fetchall()
          dq_denom = len(digitals) * 5

          all_digital_technology = []

          for item in digitals:
              all_digital_technology.append(item[0])

          next(data)
          for row in data:
                print(type(row))
                emp_id = row[12]
                empname = row[13]
                designation = row[14]
                if row[8]:
                    decimal = float(row[8].encode('utf-8').strip()) / 12
                    # print("*************",decimal)
                    decimal_rounded = round(decimal,1)
                    decimal_rounded_final = str(decimal_rounded).split('.')[1]
                    # print("*****-----------********", decimal_rounded_final)
                    years_of_experience = row[7] + "." + str(decimal_rounded_final)
                else:
                    years_of_experience = row[7]
                tech_domain = str(row[4].encode('utf-8').strip())
                tech_domain = tech_domain.replace('#', '')
                if float(years_of_experience) > 1:
                    about = str(years_of_experience) + " Years of experience across " + str(tech_domain) + " domain and working primarily on " + str(row[2].encode('utf-8').strip())
                else:
                    about = "Less than a year of experience across " + str(tech_domain) + " domain and working primarily on " + str(row[2].encode('utf-8').strip())
                try:
                    contact =float(row[0].encode('utf-8').strip())
                    contact = int(contact)
                except:
                    contact = row[0]
                functional_group = row[1]
                emp_group = fetch_emp_group(functional_group)
                if emp_group:
                    emp_groups = emp_group[0]
                    emp_functional_group = str(emp_groups)
                else:
                    emp_functional_group = 'others'
                # print("............Inserting the Functional Group Of the Employee......",emp_functional_group)
                visa = row[5]
                visa = str(visa)
                bad_chars = [';', '#', '-', '.']
                for i in bad_chars:
                    visa = visa.replace(i, ' ')

                visa_location = fetch_visalocation(visa)
                visa_data = sort_visa_location(visa_location)
                if visa_data:
                    visa_data = str(visa_data)
                    emp_visa_location = str(visa_data)
                else:
                    emp_visa_location = None
                # print("............VISA LOCATION......", emp_visa_location)
                emp_check = 0
                # CHECKING FOR THE emp data in the DB
                for emp in emp_status:
                    emp = ''.join(emp)
                    # print("!!!!", emp, "@@@@", emp_id)
                    if emp == emp_id:
                        # print("Duplication found...........NOW CHECKING FOR THE DATE ")
                        date_query = "SELECT date_of_resume,id FROM resumeparser_employee where ibs_empid = '%s';"
                        item = emp_id
                        date_result = date_query % item
                        cur.execute(date_result)
                        date_of_resumes_result = cur.fetchall()
                        emp_check = emp_check + 1
                        for date, e_id in date_of_resumes_result:
                            if date < date_of_CSV:
                                # print("Date entered is a greater than the resume date..........")
                                delete_query = "DELETE FROM resumeparser_employee_skills where employee_id=%s; DELETE FROM resumeparser_employee where id = %s;"
                                delete_db = delete_query % (e_id, e_id)
                                cur.execute(delete_db)
                                # print("AFTER THE DELETING THE OLD RECORD UPDATING IT WITH THE NEW RECORD")
                                insert_query = "INSERT INTO resumeparser_employee(ibs_empid,emp_name,designation,about,contact,status, date_of_resume, functional_group, visa, visa_location)VALUES(%s,%s,%s,%s,%s,%s, %s, %s, %s, %s)"
                                record_insert = (emp_id, empname, designation, about, contact, True, date_of_CSV, emp_functional_group, visa, emp_visa_location)
                                cur.execute(insert_query, record_insert)
                                conn.commit()
                                success = 1
                                select_id = "select id from resumeparser_employee where ibs_empid='%s'"
                                select_id = select_id % emp_id
                                cur.execute(select_id)
                                employee_id = cur.fetchall()
                                emp_id_int = employee_id[0][0]
                                # Getting the employee ID from the Resume
                                if(emp_id == 'B-3872'):
                                    print(emp_id)
                                tech = row[3]
                                new_tech = tech.replace("#", " ")
                                tech_list = new_tech.split(';')
                                new_tech_list = []
                                for tech in tech_list:
                                    if "," in tech:
                                        new_tech_list.extend(tech.split(","))
                                new_tech_list.extend(tech_list)
                                emp_tech_ids = []
                                emp_digitals_count = 0

                                for tech in new_tech_list:
                                    tech2 = tech.strip()
                                    tech_query = "select id,digital from resumeparser_technology where upper(technology) = upper('%s') "
                                    tech_query = tech_query % tech2
                                    cur.execute(tech_query)
                                    techid = cur.fetchall()
                                    ids = list(techid)
                                    for id in ids:
                                        x = "select id from resumeparser_employee_skills where employee_id='%s' and technology_id='%s'"
                                        record = (emp_id_int, id[0])
                                        cur.execute(x, record)
                                        data = cur.fetchall()
                                        if data is None or len(data) == 0:
                                            insert = "INSERT INTO resumeparser_employee_skills(employee_id,technology_id)VALUES(%s,%s)"
                                            record = (emp_id_int, id[0])
                                            cur.execute(insert, record)
                                            conn.commit()
                                        if id[1] == 'YES':
                                            emp_digitals_count = emp_digitals_count + 1
                                if emp_digitals_count > 0:
                                    dq_numerator = emp_digitals_count * 5
                                    digital_quotient = dq_numerator / dq_denom
                                    digital_quotient = digital_quotient * 100
                                    dq = round(digital_quotient, 2)
                                    update_dq_query = "UPDATE resumeparser_employee set  digital_quotient='%s' where id='%s'"
                                    record = (dq, emp_id_int)
                                    cur.execute(update_dq_query, record)
                                # print("Successful entered the new set of the emp records")
                            # else:
                                # print("OUT of the IF Loop i.e Date entered is not greater than the DB Date")
                                # print("As the date is not the latest date as compared from the resume.... NO ACTION")
                    # else:
                    #     print("When the emp_id doesnt exist..........Following the usual procedure")

                if emp_check == 0:
                    insert_query = "INSERT INTO resumeparser_employee(ibs_empid,emp_name,designation,about,contact,status, date_of_resume, functional_group, visa, visa_location)VALUES(%s,%s,%s,%s,%s,%s, %s, %s, %s, %s)"
                    record_insert = (emp_id, empname, designation, about, contact, True, date_of_CSV, emp_functional_group, visa, emp_visa_location)
                    cur.execute(insert_query, record_insert)
                    conn.commit()
                    success = 1
                    select_id = "select id from resumeparser_employee where ibs_empid='%s'"
                    select_id = select_id % emp_id
                    cur.execute(select_id)
                    employee_id = cur.fetchall()
                    emp_id_int = employee_id[0][0]
                    # Getting the employee ID from the Resume

                    tech = row[3]
                    new_tech = tech.replace("#", " ")
                    tech_list = new_tech.split(';')
                    new_tech_list =[]
                    for tech in tech_list :
                        if "," in tech :
                            new_tech_list.extend(tech.split(","))
                    new_tech_list.extend(tech_list)
                    emp_tech_ids = []
                    emp_digitals_count = 0
                    tech_error = []
                    spell_alert = 0
                    for tech in new_tech_list:
                          tech2 = tech.strip()
                          technology_data = list(Technology.objects.order_by('id'))
                          for tech_org in technology_data:
                              similarityPercent = textdistance.levenshtein.distance(tech2, str(tech_org))
                              print(similarityPercent)
                              if similarityPercent > 0 and similarityPercent < 3:
                                  spell_alert = 1
                                  break;
                          if spell_alert == 1:
                              tech2 = str(tech_org)
                          tech_query = "select id,digital from resumeparser_technology where upper(technology) = upper('%s') "
                          tech_query = tech_query % tech2
                          cur.execute(tech_query)
                          techid = cur.fetchall()
                          ids = list(techid)
                          tech_check = 0
                          tech_check = len(ids)
                          if tech_check == 0:
                              tech_error.append(tech)
                          for id in ids:
                              x = "select id from resumeparser_employee_skills where employee_id='%s' and technology_id='%s'"
                              record = (emp_id_int, id[0])
                              cur.execute(x, record)
                              data = cur.fetchall()
                              if data is None or len(data) == 0:
                                      insert = "INSERT INTO resumeparser_employee_skills(employee_id,technology_id)VALUES(%s,%s)"
                                      record = (emp_id_int, id[0])
                                      cur.execute(insert, record)
                                      conn.commit()
                              if id[1] == 'YES':
                                   emp_digitals_count = emp_digitals_count + 1
                    print("All the tech that is not inserted into the DB",tech_error,"-----------*********-------")
                    # if tech_error:
                    #     insert = "INSERT INTO resumeparser_employee_erro_log(ibs_empid,tech_error)VALUES(%s,%s)"
                    #     record = (emp_id, tech_error)
                    #     cur.execute(insert, record)
                    #     conn.commit()
                    if emp_digitals_count > 0:
                            dq_numerator = emp_digitals_count * 5
                            digital_quotient = dq_numerator / dq_denom
                            digital_quotient = digital_quotient * 100
                            dq = round(digital_quotient,2)
                            update_dq_query = "UPDATE resumeparser_employee set  digital_quotient='%s' where id='%s'"
                            record = (dq, emp_id_int)
                            cur.execute(update_dq_query,record)
                            conn.commit()
                    # print("Successful entered the new set of the emp records")
    return success
def buildPatterns(skills):
    pattern = []
    for skill in skills:
        pattern.append(skillPattern(skill))
    return list(zip(skills, pattern))

def skillPattern(skill):
    pattern = []
    for b in skill.split():
        pattern.append({'LOWER':b})
    return pattern

def SentenceMatcher(phrase_matcher, text):

    doc = nlp_sm((text.lower()))
    matches = phrase_matcher(doc)
    match_list =[]
    for b in matches:
        match_id, start, end = b
        match_list.append(str(doc[start: end]))
    return set(match_list)

def on_match(matcher, doc, id, matches):
    return matches


def empGroupbuildMatcher(patterns):

    phrase_matcher = Matcher(nlp_sm.vocab)
    for pattern in patterns:
        phrase_matcher.add(pattern[0], on_match, pattern[1])
    return phrase_matcher

def fetch_emp_group(doc):

    cur.execute("""SELECT * FROM public.emp_group;""")
    emp_groups = cur.fetchall()
    emp_groups_array = []

    for empgroup in emp_groups:
        emp_groups_list = []
        if empgroup[0]:
            emp_group_name = empgroup[0].rstrip().lower()
            emp_groups_array.append(emp_group_name)

    empgroup_patterns = buildPatterns(emp_groups_array)

    emp_group_matcher = empGroupbuildMatcher(empgroup_patterns)

    emp_groups_list.extend(SentenceMatcher(emp_group_matcher, doc))
    return emp_groups_list


def airlinebuildMatcher(patterns):

    phrase_matcher = Matcher(nlp_sm.vocab)
    for pattern in patterns:
        phrase_matcher.add(pattern[0], on_match, pattern[1])
    return phrase_matcher



def fetch_visalocation(doc):

    doc_text = nlp_sm(doc)
    airline_list = []
    airline_matcher = Matcher(nlp_sm.vocab)
    cur.execute("""SELECT * FROM public.visa_location;""")
    airline_codes = cur.fetchall()
    airline_code_array = []
    airline_name_array= []
    for airline_code in airline_codes:
        airline_code_list = []
        if airline_code[0]:
            airline_code_list.append({'LOWER': (airline_code[0]).lower()})
            airline_code_array.append(airline_code_list)
        if airline_code[1]:
            airline_names = airline_code[1].rstrip().lower()
            airline_name_array.append(airline_names)

    phrase_patterns = airline_code_array
    airline_matcher.add('Airline_Code', None, *phrase_patterns)
    airline_matches = airline_matcher(doc_text)
    for match_id, start, end in airline_matches:
        string_id = nlp_sm.vocab.strings[match_id]  # get string representation
        span = doc_text[start:end]  # the matched span
        airline_list.append(span.text)

    airline_patterns = buildPatterns(airline_name_array)

    airline_matcher = airlinebuildMatcher(airline_patterns)

    airline_list.extend(SentenceMatcher(airline_matcher, doc))
    return airline_list

def sort_visa_location(data):
    visa_location_output = list(data)
    visa_location_output = [item.lower().strip() for item in visa_location_output]
    visa_location_output = set(visa_location_output)
    visa_location_code = list(visa_location_output)
    cur.execute("""SELECT * FROM public.visa_location;""")
    airline_codes = cur.fetchall()
    airline_code_array = []
    airline_name_array= []
    for airline_code in airline_codes:
        airline_code_list = []
        if airline_code[0]:
            airline_code_list = airline_code[0].strip().lower()
            airline_code_array.append(airline_code_list)
        if airline_code[1]:
            airline_names = airline_code[1].strip().lower()
            airline_name_array.append(airline_names)
    visa_dict = dict(zip(airline_code_array,airline_name_array))
    # print("#######...NEW DICTIONARY FORMED",visa_dict,"^^^^^^^^^LIST TO BE COMPARED WITH",visa_location_code)
    output = [visa_dict.get(key) for key in visa_location_code if visa_dict.get(key)]
    output = set(output)
    output_final = [str(i) for i in output]
    output_final_data = (",".join(output_final))
    return output_final_data