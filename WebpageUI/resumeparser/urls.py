from django.urls import path
from .views import search_tehnology,get_employees_with_skills,tablepage, get_techs,multisel,searchtable,skill, add_technology, technologyList, searchEmployee, filterParametersList,fileUpload, getAllTechnologies,getDevEmployees,getTestingEmployees
from .views import get_employee_profile_details,check_page,runcheck,get_employee_with_name,get_employee_with_id, runbot, dashboard, runcheckCSV, add_technology_page_form, add_technology_db
from .views import addTechnology, getEmployeeByID, getEmployeeProfile,getAllVisaLocations,getAllGraphs,updateGraph
from .IBSEmployeePortal.apis.ibs_employee_data_apis import getEmployeeSkills
from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = [
    path('index',search_tehnology, name='in'), # - same as search
    path('search', search_tehnology, name='MyApps'), #- replaced
    path('TechnologyAutoComplete', getEmployeeSkills, name='get-employee-skills'), #- replaced
    path('GetEmployeeWithSkills', get_employees_with_skills, name='search-employee-by-skills'), #- replaced
    # path('addtechnology',add_technology_page, name='addtechnology'),
    path('tablepage', tablepage, name='table_page'), # - not working
    path('gettechs',get_techs,name='get_techs'),# - dummy
    path('multisel',multisel,name='multiselect'),# - dummy
    path('searchtable',searchtable, name='search_table'),# - dummy
    path('skill',skill,name='skill'),
    # path('addtech',add_technology, name='add_technology_code'),
    path('add_technology', add_technology, name='add_technology_code'),
    path('amchart',get_employee_profile_details, name='getprofiles'),#- replaced
    path('check',check_page,name='checkpage'),# not required
    path('runcheck',runcheck, name='run_check'),#- replaced
    path('runcheckCSV', runcheckCSV, name='run_checkCSV'),#- replaced
    path('GetEmployeeWithName',get_employee_with_name, name='get-with-name'),
    path('GetEmployeeWithId', get_employee_with_id, name='get-with-id'),#- replaced
    path('runbot',runbot,name='runbot'),#- replaced
    path('dashboard',dashboard,name="HomePage"),#- replaced
    path('add_technology_page', add_technology_page_form, name="Add_Technology_Page"),#- replaced
    path('add_technology_save', add_technology_db, name="Add_Technology_Save"),#- replaced
    # --------------------- vigil ------------------------
    path('getAllTechnologies', technologyList.as_view(), name="getAllTechnologies"),
    path('searchEmployees', searchEmployee.as_view(), name='searchEmployees'),
    path('filterParameters', filterParametersList.as_view(), name='filterParameters'),
    path('parseFile', fileUpload.as_view(), name='parseFile'),
    path('getAllTechnologies', getAllTechnologies, name="getAllTechnologies"),
    path('addTechnology', addTechnology.as_view(), name="addTechnology"),
    path('getEmployeeByID', getEmployeeByID.as_view(), name="getEmployeeByID"),
    path('getUserProfile', getEmployeeProfile.as_view(), name='getUserProfile'),
    # -------------Nayana------------------------
    path('getTestingEmployees', getTestingEmployees.as_view(), name="getTestingEmployees"),
    path('getDevEmployees', getDevEmployees.as_view(), name="getDevEmployees"),
    path('getAllGraphs', getAllGraphs.as_view(), name="getAllGraphs"),
    path('updateGraphs', updateGraph.as_view(), name="updateGraphs"),

    path('getAllVisaLocations', getAllVisaLocations.as_view(), name="getAllVisaLocations")

]
