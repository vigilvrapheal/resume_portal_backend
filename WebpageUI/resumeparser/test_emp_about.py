import spacy


def fetch_emp_about(doc):
    emp_year = []
    emp_info = []
    nlp_room = spacy.load("models/base/emp_about")
    # print("Inside")
    doc = nlp_room(doc)
    for ent in doc.ents:
        if ent.label_ == 'Year Of Experience':
            year_exp = ent.text
            # print(year_exp,"*********")
            if year_exp:
                emp_year.append(year_exp)
        if ent.label_ == 'About Info':
            about_info = ent.text
            # print(about_info,"----------")
            if about_info:
                emp_info.append(about_info)
    return emp_year, emp_info

text = "Hardworking, result-oriented and process-driven senior software engineer with 4 years of experience in Core Java and Oracle ERP.Design and develop application based on Core Java.Write unit and system test plan for the application.Designing, developing and reviewing functional/ technical documents.Development work in HRMS/Payroll module of ERP.Development of Interfaces (HRMS/Payroll related).Researching, consulting, analysing and evaluating system program needs.Worked on automated testing of Rest based web services."
text2 = "3+ months experience in both front end and backend development and frameworks. 3+ years experience in both " \
        "front end and backend development and frameworks."
text3 = "10 years of the IT industry with lots of experience in the Backend part."
result = fetch_emp_about(text3)

# print("****", result)