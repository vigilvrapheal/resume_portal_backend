# Generated by Django 2.2.1 on 2019-06-24 07:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AppType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app_type_name', models.CharField(blank=True, max_length=200, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Technology',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('technology', models.CharField(blank=True, max_length=200, null=True)),
                ('digital', models.CharField(blank=True, max_length=200, null=True)),
                ('category', models.CharField(blank=True, max_length=200, null=True)),
                ('status', models.BooleanField(default=True)),
                ('app_final', models.CharField(blank=True, max_length=200, null=True)),
                ('apptype', models.ManyToManyField(related_name='technology_list', to='resumeparser.AppType')),
            ],
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ibs_empid', models.CharField(blank=True, max_length=20, null=True)),
                ('emp_name', models.CharField(blank=True, max_length=200, null=True)),
                ('designation', models.CharField(blank=True, max_length=200, null=True)),
                ('about', models.CharField(blank=True, max_length=100000, null=True)),
                ('contact', models.CharField(blank=True, max_length=100000, null=True)),
                ('status', models.BooleanField(default=True)),
                ('digital_quotient', models.FloatField(blank=True, default=None, null=True)),
                ('skills', models.ManyToManyField(related_name='emplist_list', to='resumeparser.Technology')),
            ],
        ),
    ]
