#import necessary modules

from .models import AppType, Technology

def readTechnologyCSV():

    import csv

    reader = csv.DictReader(open("final_classification.csv"))

    app_type_dict_list = []
    technology_dict_list = []

    for technology_raw in reader:
        print(technology_raw['Technology'])

        app_type_str = str(technology_raw['App Type'])
        app_type_obj = AppType.objects.filter(app_type_name__icontains=app_type_str)

        if app_type_obj != None:

           app_type_obj = app_type_obj[0]

           if app_type_obj == None:

              AppType.objects.create(app_type_name=app_type_str)



# class AppType(models.Model):
#
#     app_type_name = models.CharField(max_length=200, null=True, blank=True)
#
#     def __str__(self):
#         return self.category_name
#
# class Technology(models.Model):
#     """
#         Model for store Technology
#     """
#     technology = models.CharField(max_length=200, null=True, blank=True)
#     digital = models.CharField(max_length=200, null=True, blank=True)
#     apptype = models.ManyToManyField(AppType, related_name='technology_list')
#     category =  models.CharField(max_length=200, null=True, blank=True)
#     status = models.BooleanField(default=True)
#     app_final= models.CharField(max_length=200, null=True, blank=True)
#
#     def __str__(self):
#         return self.technology


# class CSVHandler:
#
#     def readCSVWithPath(self, file_path):
#
#         import csv
#
#         reader = csv.DictReader(open("final_classification.csv"))
#         csv_row_list = []
#
#         for raw in reader:
#             csv_row_list.append(raw)
#
#         return csv_row_list



readTechnologyCSV()

