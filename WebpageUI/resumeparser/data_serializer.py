from rest_framework import serializers
from .models import Technology
from .models import Graphs

class TechnologySerializer(serializers.ModelSerializer):

    technology = serializers.CharField(source='technology')
    digital = serializers.CharField(source='digital')
    apptype = serializers.CharField(source='apptype')
    category = serializers.CharField(source='category')
    status= serializers.CharField(source='status')
    app_final = serializers.CharField(source='app_final')

    class Meta:
        model = Technology
        # STANDARD WAY
        # fields = ("technology", "digital", "apptype", "category", "status", "app_final")

        # TRADITIONAL WAY TO CHANGE KEY/PARAMETER NAME
        fields = ("technology", "digital", "apptype","category","status","app_final")

class GraphsSerializer(serializers.ModelSerializer):

    graph_id = serializers.CharField()
    graph_name = serializers.CharField()
    status = serializers.BooleanField()

    class Meta:
        model = Graphs
        fields = ("graph_id", "graph_name","status")