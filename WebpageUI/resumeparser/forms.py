from django import forms
from .models import Technology
from .models import Graphs


class AddTechForm(forms.ModelForm):
    class Meta:
        model = Technology
        fields = ['technology', 'digital', 'category', 'status', 'app_final']


class AddGraphForm(forms.ModelForm):
    class Meta:
        model = Graphs
        fields = ['graph_id', 'graph_name', 'status']
