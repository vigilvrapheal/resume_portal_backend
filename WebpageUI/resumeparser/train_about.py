from __future__ import unicode_literals, print_function

import plac
import random
from pathlib import Path
import spacy
from spacy.util import minibatch, compounding


# new entity label
LABEL_YEAR = 'Year Of Experience'
LABEL_INFO = 'About Info'

# training data
# Note: If you're using an existing model, make sure to mix in examples of
# other entity types that spaCy correctly recognized before. Otherwise, your
# model might learn the new type, but "forget" what it previously knew.
# https://explosion.ai/blog/pseudo-rehearsal-catastrophic-forgetting
TRAIN_DATA = [
    ("2+ years of IT industry experience in middleware development.",{'entities': [(0, 8, 'Year Of Experience'), (38, 60, 'About Info')]}),
    ("1+ years of IT industry experience in the software development activities comprising requirement analysis, design, coding and code review.", {'entities': [(0, 8, 'Year Of Experience'), (42, 137, 'About Info')]}),
    ("2+ years of IT industry experience in development using the technologies Java/JEE, Spring, Struts, JSP, JQuery, JSF, JPA, Liferay, NodeJs and Oracle Apex.", {'entities': [(0, 8, 'Year Of Experience'), (38, 153, 'About Info')]}),
    ("Currently working as Software Engineer with EGENCIA, EXPEDIA.", {'entities': [(44, 60, 'About Info')]}),
    ("Quality driven development professional with 2 years of experience in core Java,Spring and Microservice development",
    {'entities': [(45, 52, 'Year Of Experience'), (71, 116, 'About Info')]}),
    ("5+ Years of IT Experience in development, Enhancement and Maintenance roles using java technologies", {'entities': [(0, 8, 'Year Of Experience'), (29, 99, 'About Info')]}),
    ("5+ years of IT industry experience in java/j2ee technologies for medium/large scale projects.", {'entities': [(0, 8, 'Year Of Experience'), (38, 92, 'About Info')]}),
    ("6.4 years of IT industry experience in various technologies like Java, J2EE, JDBC, Struts2, Spring, Hibernate, Web Services.", {'entities': [(0, 9, 'Year Of Experience'), (39, 123, 'About Info')]}),
    ("8+ years of IT industry experience in requirement gathering, software development and implementation.",{'entities': [(0, 8, 'Year Of Experience'), (38, 100, 'About Info')]}),
    ("7+ years of IT industry experience in Java\\J2EE development projects.", {'entities': [(0, 8, 'Year Of Experience'), (38, 69, 'About Info')]}),
    ("7+ years of IT industry experience in mobile application development using native and hybrid platforms.", {'entities': [(0, 8, 'Year Of Experience'), (38, 101, 'About Info')]}),
    ("1+ years of IT industry experience.", {'entities': [(0, 8, 'Year Of Experience')]}),
    ("3+ years of IT industry experience as a Full-stack developer in services and product based architecture platform.", {'entities': [(0, 8, 'Year Of Experience'), (40, 112, 'About Info')]}),
    ("2+ years of IT industry experience in software development activities comprising requirement analysis, coding, bug fixes, implementation, code review and enhancements.",{'entities': [(0, 8, 'Year Of Experience'), (38, 166, 'About Info')]}),
    ("4+ years of IT industry experience in solution feasibility analysis, project management, business analysis, consulting for medium/large scale projects.",{'entities': [(0, 8, 'Year Of Experience'), (38, 150, 'About Info')]}),
    ("3+ years of IT industry experience in logistics domain handling development and maintenance for freight movement of a North American Railroad.",{'entities': [(0, 8, 'Year Of Experience'), (38, 141, 'About Info')]}),
    ("3+ years of IT industry experience in Application development.",{'entities': [(0, 8, 'Year Of Experience'), (38, 61, 'About Info')]}),
    ("3.9 years of IT industry experience in developing web, enterprise and mobile app based applications in Telecom, Ecommerce and Loyalty Domains.",{'entities': [(0, 9, 'Year Of Experience'), (39, 141, 'About Info')]}),
    ("3+ years of IT industry experience in development and implementation of enterprise applications using JAVA, J2EE.",{'entities': [(0, 8, 'Year Of Experience'), (38, 112, 'About Info')]}),
    ("Quality driven development professional with 2+ years of experience in core Java,Spring and Microservice development", {'entities': [(45, 53, 'Year Of Experience'), (71, 116, 'About Info')]}),
    ("7 months experience in creation and execution of test case scenarios.",{'entities': [(0, 8, 'Year Of Experience'), (23, 68, 'About Info')]}),
	("6+  years of IT industry experience",{'entities': [(0, 9, 'Year Of Experience')]}),
    ("Currently working as Software Engineer with NIKE, EXPEDIA.",{'entities': [(44, 57, 'About Info')]}),
	("2.5 years of experience in various phases of Software Development Life Cycle, particularly focusing on backend development.",{'entities': [(0, 9, 'Year Of Experience'), (27, 122, 'About Info')]})
]



@plac.annotations(
    model=("Model name. Defaults to blank 'en' model.", "option", "m", str),
    output_dir=("Optional output directory", "option", "o", Path),
    n_iter=("Number of training iterations", "option", "n", int),
)


def train_entity(model=None, new_model_name='emp_about', output_dir=None, n_iter=10):
    """Set up the pipeline and entity recognizer, and train the new entity."""

    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.load('en_core_web_sm')  # create blank Language class
        print("Created blank 'en' model")
    # Add entity recognizer to model if it's not in the pipeline
    # nlp.create_pipe works for built-ins that are registered with spaCy
    if 'ner' not in nlp.pipe_names:
        ner = nlp.create_pipe('ner')
        nlp.add_pipe(ner)
    # otherwise, get it, so we can add labels to it
    else:
        ner = nlp.get_pipe('ner')

    ner.add_label(LABEL_YEAR)   # add new entity label to entity recognizer
    ner.add_label(LABEL_INFO)

    if model is None:
        optimizer = nlp.begin_training()
    else:
        # Note that 'begin_training' initializes the models, so it'll zero out
        # existing entity types.
        optimizer = nlp.entity.create_optimizer()

    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != 'ner']
    with nlp.disable_pipes(*other_pipes):  # only train NER
        for itn in range(n_iter):
            random.shuffle(TRAIN_DATA)
            losses = {}
            # batch up the examples using spaCy's minibatch
            batches = minibatch(TRAIN_DATA, size=compounding(4., 32., 1.001))
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer, drop=0.35,
                           losses=losses)
            print('Losses', losses)

    # test the trained model
    test_text = '3.5 years of IT industry experience in in Java (J2EE) software development life cycle'
    doc = nlp(test_text)
    print("Entities in '%s'" % test_text)
    for ent in doc.ents:
        print(ent.label_, ent.text)

    # save model to output directory
    # print('output_dir')
    output_dir ='models/base/emp_about'
    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp.meta['name'] = new_model_name  # rename model
        nlp.to_disk(output_dir)
        print("Saved model to", output_dir)

        # test the saved model
        print("Loading from", output_dir)
        nlp2 = spacy.load(output_dir)
        doc2 = nlp2(test_text)
        for ent in doc2.ents:
            print(ent.label_, ent.text)


if __name__ == '__main__':
    plac.call(train_entity)



