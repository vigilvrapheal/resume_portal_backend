from django.contrib import admin
from resumeparser.models import *
admin.site.register(Employee)
admin.site.register(Technology)
admin.site.register(Graphs)
