

$(document).ready(function () {

    $("#menu-toggle").click(function (e) {
        
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    $(window).resize(function () {

        var page_width = $(window).width();

        if (page_width < 992) {

            if ($('#wrapper').hasClass("toggled")) {
                $("#wrapper").toggleClass("toggled");
                $(".ishop-nav-toggle-item").css('display', 'block');
            }

        }
        else if ($('#wrapper').hasClass("toggled") == false) {

            $("#wrapper").toggleClass("toggled");
            $(".ishop-nav-toggle-item").css('display', 'none');

        }

    });
});