// Main Function for fetching the details of the employees
function get_employee_list_filter(segment_no, page_no, skill_ids, search_text, filters) {
dep_ajax = null
visa_ajax = null
if(filters != null){
  dep_ajax = filters[0];
  visa_ajax = filters[1];
  }
  $('#empresult').hide();
  console.log(dep_ajax,"&&&&&&&&&",visa_ajax)
  console.log("###############",skill_ids)
  $.ajax({

    url: 'GetEmployeeWithSkills',
    data: {
      'page_no': page_no ,
      'segment_no' : segment_no ,
      'skill_ids': skill_ids,
      'search_text': search_text,
      'departments': dep_ajax,
      'visa': visa_ajax
    },

    success: function (data) {
      console.log("The length of the data of the employees as per the result", data.length, "********--", data)
      $("#box").empty().append(data);
      $('#empresult').show();
      $('#filters_body').show();
  }
  });
}

// Main Function for fetching the details of the employees after applying the filters
