

    $(document).ready(function() {

      $("#box").on("click", ".page-next", function (e) {

      seg_no =  e.target.name;
      pg_no = ( seg_no - 1 ) * $("input[name=segment_size]").val() + 1;

      getComments(seg_no, pg_no);

    });

     $("#box").on("click", ".page-prev", function (e) {

      seg_no =  e.target.name;
      pg_no = ( seg_no - 0 ) * $("input[name=segment_size]").val() + 0 ;
      pid = $("input[name=productID]").val();

      getComments(seg_no, pg_no);

    });

    $("#box").on("click", ".page-current", function (e) {

      seg_no =  $("input[name=segment_no]").val();
      pg_no = e.target.name;
      pid = $("input[name=productID]").val();

      getComments(seg_no, pg_no);

    });

     $("#box").on("click", ".page-first", function (e) {

      seg_no =  1;
      pg_no = 1;
      pid = $("input[name=productID]").val();

      getComments(seg_no, pg_no);

    });

     $("#box").on("click", ".page-last", function (e) {

      console.log('&&&&&&');
      seg_no =  $("input[name=total-count]").val();
      pg_no = $("input[name=page-last]").val();
      pid = $("input[name=productID]").val();
      console.log(pg_no,"************",seg_no)
      getComments(seg_no, pg_no);

    });

    function getComments(segment_no, page_no) {

      console.log('segment: ', segment_no);
      console.log('page: ', page_no);
//      <!--console.log('product_id: ', product_id);-->

      $.ajax({

        url: '{% url "get_techs" %}',
        data: {
          'page_no': page_no ,
          'segment_no' : segment_no
        },

        success: function (data) {

          $("#box").empty().append(data);
        }
      });
    }

 });
