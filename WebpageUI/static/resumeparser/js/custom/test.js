$(document).ready(function() {

    $( "#searchbutton" ).click(function() {
      get_employee_list(1, 1, get_selected_skills(), $('#search_bar').val());

    });

	// function for the NEXT Page Function
     $("#box").on("click", ".page-next", function (e) {

      seg_no =  e.target.name;
      pg_no = ( seg_no - 1 ) * $("input[name=segment_size]").val() + 1;

      get_employee_list(seg_no, pg_no, get_selected_skills(), $('#search_bar').val());

    });

	// function for the PREVIOUS Page Function
     $("#box").on("click", ".page-prev", function (e) {

      seg_no =  e.target.name;
      pg_no = ( seg_no - 0 ) * $("input[name=segment_size]").val() + 0 ;
      pid = $("input[name=productID]").val();

      get_employee_list(seg_no, pg_no, get_selected_skills(), $('#search_bar').val());

    });

	// function for the CURRENT Page Function
    $("#box").on("click", ".page-current", function (e) {

      seg_no =  $("input[name=segment_no]").val();
      pg_no = e.target.name;
      pid = $("input[name=productID]").val();

      get_employee_list(seg_no, pg_no, get_selected_skills(), $('#search_bar').val());

    });

	// function for the FIRST Page Function
     $("#box").on("click", ".page-first", function (e) {

      seg_no =  1;
      pg_no = 1;
      pid = $("input[name=productID]").val();

      get_employee_list(seg_no, pg_no, get_selected_skills(), $('#search_bar').val());

    });

	// function for the LAST Page Function
     $("#box").on("click", ".page-last", function (e) {

      console.log('&&&&&&');
      seg_no =  $("input[name=total-count]").val();
      pg_no = $("input[name=page-last]").val();
      pid = $("input[name=productID]").val();

      get_employee_list(seg_no, pg_no, get_selected_skills(), $('#search_bar').val());

    });

	// Main Function for fetching the details of the employees
    function get_employee_list(segment_no, page_no, skill_ids, search_text) {
	  console.log("....Printing the parameters of the get_employee_list function....")
      console.log('Segment No: ', segment_no);
      console.log('Page No: ', page_no);
	  console.log('Skill_ids: ', skill_ids);
	  console.log('Search_text: ', search_text);


      $.ajax({

        url: 'GetEmployeeWithSkills',
        data: {
          'page_no': page_no ,
          'segment_no' : segment_no ,
          'skill_ids': skill_ids,
          'search_text': search_text
        },

        success: function (data) {

          $("#box").empty().append(data);
          $('#empresult').show();
        }
      });
    }
    // Main Function for fetching the details of the employees after applying the filters

 });