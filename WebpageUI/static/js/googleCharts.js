google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawLineChart);
var voice_data = [
      ['Year', 'Frankfurt', 'Berlin' ,'Tomorrow'],
      ['12AM',  1000,      400 , 300],
      ['4 AM',  660,       1120 , 350],
      ['8 AM',  1030,      540 , 150],
      ['12 PM',  1030,      540 , 100],
      ['4 PM',  1030,      540, 300],
      ['8 PM',  1030,      540, 300 ],
    ];
 var chat_data = [
      ['Year', 'Weather', 'Izmir' ,'Tomorrow'],
      ['12AM',  1000,      400 , 300],
      ['4 AM',  660,       500 , 500],
      ['8 AM',  1030,      600 , 800],
      ['12 PM',  1030,      700 , 1000],
      ['4 PM',  1030,      800, 300],
      ['8 PM',  1030,      900, 300 ],
    ];
var mail_data =  [
      ['Year', 'Miles', 'Dubai' ,'Miami'],
      ['12AM',  1000,      400 , 300],
      ['4 AM',  660,       200 , 350],
      ['8 AM',  1030,      300 , 150],
      ['12 PM',  1030,      1000 , 100],
      ['4 PM',  1030,      1000, 300],
      ['8 PM',  1030,      1000, 300 ],
    ];
var review_data = [
      ['Year', 'Delay', 'Cancelled' ,'Crew'],
      ['12AM',  1000,      400 , 300],
      ['4 AM',  660,       1120 , 350],
      ['8 AM',  1030,      540 , 150],
      ['12 PM',  1030,      540 , 100],
      ['4 PM',  1030,      540, 300],
      ['8 PM',  1030,      540, 300 ],
    ];

function drawLineChart(scenario){
        var data =[];
        switch(scenario){
            case 'chat':
                data = chat_data;
                break;
             case 'mail':
                data = mail_data;
                break;
             case 'voice':
                data = voice_data;
                break;
             case 'review':
                data = review_data;
                break;
             default:
                data = voice_data;
                break;
        }

    var options = {
      title: 'Trending Last 24 Hours',
      curveType: 'function',
        legend: { position: 'bottom' },
        height: 350,
    };
    var data = google.visualization.arrayToDataTable(data);
    var chart = new google.visualization.LineChart(document.getElementById('voice_chart_div'));

    chart.draw(data, options);
}
