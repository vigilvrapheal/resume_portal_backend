
// Themes end

 // Create map instance
var chart = am4core.create("map_chart", am4maps.MapChart);

// Set map definition
chart.geodata = am4geodata_worldLow;
// Set projection
chart.projection = new am4maps.projections.Miller();
chart.seriesContainer.draggable = false;
chart.seriesContainer.resizable = false;
chart.maxZoomLevel = 1;

// Create map polygon series
var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

//Set min/max fill color for each area
polygonSeries.heatRules.push({
  property: "fill",
  target: polygonSeries.mapPolygons.template,
  min: chart.colors.getIndex(1).brighten(1),
  max: chart.colors.getIndex(1).brighten(-0.3)
});

// Make map load polygon data (state shapes and names) from GeoJSON
polygonSeries.useGeodata = true;
polygonSeries.include = ["PT", "ES", "FR", "DE", "BE", "NL", "IT", "AT", "GB", "IE", "CH", "LU"];
 var min=100;
 var max=1000;
// Set heatmap values for each state
polygonSeries.data = [
     {    id: "AT",
        value: Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "IE",
       value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "DK",
       value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "FI",
        value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "SE",
        value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "GB",
        value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "IT",
       value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "FR",
        value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "ES",
       value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "GR",
        value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "DE",
       value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "BE",
         value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "LU",
         value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "NL",
         value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "PT",
         value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "LT",
        value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "LV",
         value: Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "CZ",
         value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "SK",
        value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "SI",
        value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "EE",
         value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "HU",
         value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "CY",
         value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "MT",
         value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "PL",
        value:  Math.round(Math.random() * (+max - +min) + +min)
      },{
        id: "RO",
         value: Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "BG",
         value:  Math.round(Math.random() * (+max - +min) + +min)
      }, {
        id: "HR",
         value:  Math.round(Math.random() * (+max - +min) + +min)
      }

];

// Set up heat legend
let heatLegend = chart.createChild(am4maps.HeatLegend);
heatLegend.series = polygonSeries;
heatLegend.align = "right";
heatLegend.valign = "bottom";
heatLegend.width = am4core.percent(20);
heatLegend.marginRight = am4core.percent(4);
heatLegend.minValue = 0;
heatLegend.maxValue = 40000000;

// Set up custom heat map legend labels using axis ranges
var minRange = heatLegend.valueAxis.axisRanges.create();
minRange.value = heatLegend.minValue;
minRange.label.text = "Positive";
var maxRange = heatLegend.valueAxis.axisRanges.create();
maxRange.value = heatLegend.maxValue;
maxRange.label.text = "Negative";

// Blank out internal heat legend value axis labels
heatLegend.valueAxis.renderer.labels.template.adapter.add("text", function(labelText) {
  return "";
});

// Configure series tooltip
var polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.tooltipText = "{name}: {value}";
polygonTemplate.nonScalingStroke = true;
polygonTemplate.strokeWidth = 0.5;

// Create hover state and set alternative fill color
var hs = polygonTemplate.states.create("hover");
hs.properties.fill = am4core.color("#3c5bdc");

